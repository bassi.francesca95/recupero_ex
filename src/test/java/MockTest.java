//aiuto per mock:
//https://www.javaworld.com/article/3537563/junit-5-tutorial-part-1-unit-testing-with-junit-5-mockito-and-hamcrest.html?page=3

import bday_service.*;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.List;

import static org.mockito.Mockito.*;

public class MockTest {
    @Mock
    Employee employee;
    @Mock
    EmployeeRepository employeeRepository;
    @Mock
    SendMails sendMails;

    @Before
    public void Mockinit()
    {
        MockitoAnnotations.initMocks(this);
    }


/*
    static Employee employee;
    static EmployeeRepository employeeRepository;
    static SendMails sendMails;

    @BeforeAll
    static void MockInit(){
        employee = mock(Employee.class);
        employeeRepository = mock(EmployeeRepository.class);
        sendMails = mock(SendMails.class);
    }

*/
    private Employee build_emp(int day, int month, int year){
        return new Employee ("pasticcino", "crema", "past@gmail.com",day, month, year );
    }

    @Test
    void findEmployee(){
        when(employeeRepository.findEmployeesBornOn(any(LocalDate.class))).thenReturn(List.of(build_emp(1,9,1980), build_emp(1,9,1988)));

        //employeeRepository.findEmplyeesBordOn(LocalDate.of(2000, 9,1));
       // assertTrue(employeeRepository.findEmplyeesBordOn(LocalDate.of(2000, 9,1)).size() > 0);
    }

    @Test
    void checkmockTest(){
        when(employee.getName()).thenReturn("pasticcino");
        assertEquals("pasticcino", employee.getName());
    }

    @Test
    void bdayin_09_02(){
        Employee employee1 = new Employee("pasticcino", "cosetto", "pasticcino@gmail.com", 9, 2, 1996);
        assertTrue(employee1.isBday(LocalDate.of(2004, 2, 9)));
    }
    @Test
    void bdayin_29_02(){
        Employee employee1 = new Employee("pasticcino", "cosetto", "pasticcino@gmail.com", 29, 2, 1996);
        assertTrue(employee1.isBday(LocalDate.of(2003, 2, 28)));
    }

    @Test
    void bdayMock(){
        when(employee.isBday(any(LocalDate.class))).thenReturn(true);
        assertTrue(employee.isBday(any(LocalDate.class)));
    }
}
