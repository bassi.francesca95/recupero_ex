package bday_service;

import java.time.LocalDate;

public class Employee {
    private LocalDate birthday;
    private String name;
    private String surname;
    private String email;

    public Employee(String name, String surname, String email, int day, int month, int year)
    {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.birthday = LocalDate.of(year, month, day);
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public boolean isBday(LocalDate localDate){
        if (birthday.getMonthValue() == localDate.getMonthValue() && birthday.getDayOfMonth() == localDate.getDayOfMonth())
            return true;

        if (birthday.getMonthValue() == 2 && birthday.getDayOfMonth() == 29 && !localDate.isLeapYear()) {
            return (localDate.getMonthValue() == 2 && localDate.getDayOfMonth() == 28);
        }

        return false;
    }

}
