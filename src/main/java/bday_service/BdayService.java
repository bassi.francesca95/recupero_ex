package bday_service;

import java.time.LocalDate;
import java.util.List;

public class BdayService {
    EmployeeRepository repository;
    SendMails sendMails;

    public BdayService(EmployeeRepository er, SendMails s)
    {
        repository = er;
        sendMails = s;
    }

    public void sendGreetings(int month, int day, int year) {
        List<Employee> employee = repository.findEmployeesBornOn(LocalDate.of(year, month, day));
        for (Employee value : employee)
            sendMails.send(value.getEmail(), "happy bday!", "Happy bday dear!" + value.getName());
    }

}
