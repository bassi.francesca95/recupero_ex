package bday_service;

import java.time.LocalDate;
import java.util.List;

public interface EmployeeRepository {
    List<Employee> findEmployeesBornOn(LocalDate localDate);
}
